from flask import Flask, jsonify, request
from iotery_python_server_sdk import Iotery
import os, time

iotery = Iotery("ZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5LmV5SnpkV0lpT2lJNE1tWTNNVEF6TkMxbU1qRmxMVEV4WlRrdFlqVTBPQzFrTWpnek5qRXdOall6WldNaUxDSnBZWFFpT2pFMU56RTBOVFE0TkRjc0ltVjRjQ0k2TkRjeU56SXhORGcwTjMwLnpGVnFqRmtnMTJyYW5DQkpSM1BCdVNHUVMxR1BFaENVVzJ5RHFrNzNpSkE=")
app = Flask(__name__)

PORT = os.getenv("PORT")

time = time.time()

# http://localhost:9998/hello
@app.route("/hello/<name>", methods=["GET"])
def get_hello(name):
    return jsonify({"hello":name, "timestamp":time})

@app.route("/number/<integer>", methods=["POST"])
def create_number(integer):
    body = request.json
    return jsonify({"result":integer, "body":body})

@app.route("/devices/<device_uuid>/command-types/<command_type_uuid>", methods=["POST"])
def create_command(device_uuid, command_type_uuid):
    res = iotery.createDeviceCommandInstance(deviceUuid="8ae50c3e-f9b1-11e9-b548-d283610663ec", data={"commandTypeUuid": "2a4dce0a-f9c1-11e9-b548-d283610663ec"})
    return jsonify({"device_uuid":device_uuid, "command_type_uuid":command_type_uuid})

# @app.route("/devices/<device_uuid>/command-types/<command_type_uuid>", methods=["POST"])
# def create_command(device_uuid, command_type_uuid):
#    res = iotery.createDeviceCommandInstance(deviceUuid="8ae50c3e-f9b1-11e9-b548-d283610663ec", data={"commandTypeUuid": "2a4dce0a-f9c1-11e9-b548-d283610663ec"})
#    return jsonify({"device_uuid":device_uuid, "command_type_uuid":command_type_uuid})

if __name__ == "__main__":
    app.run(debug=True, port=PORT, host="0.0.0.0")