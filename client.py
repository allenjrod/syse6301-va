import requests
import json


r = requests.post("http://localhost:9998/number/15", 
                  headers={"Content-Type":"application/json"}, 
                  data=json.dumps({"units":"kg", "name":"hello"}))

print(r.json())